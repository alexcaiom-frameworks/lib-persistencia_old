package br.com.massuda.alexander.persistencia.jdbc.finder;

public enum TipoFiltro {

	/**
	 * A = B
	 */
	IGUAL_A				("="),
	/**
	 * A <> B
	 */
	DIFERENTE_DE		("<>"),
	/**
	 * A > B
	 */
	MAIOR_QUE			(">"),
	/**
	 * A < B
	 */
	MENOR_QUE			("<"),
	/**
	 * A >= B
	 */
	MAIOR_OU_IGUAL_A	(">="),
	/**
	 * A <= B
	 */
	MENOR_OU_IGUAL_A	("<="),
	/**
	 * A like B
	 */
	COMO				(" like "),
	/**
	 * in (A,  B)
	 */
	EM		(" in "),
	/**
	 * between A and B
	 */
	ENTRE		(" between {a} and {b} ");
	
	private String textoCondicional;
	
	private TipoFiltro(String textoCondicional) {
		this.textoCondicional  = textoCondicional;
	}

	public String getTextoCondicional() {
		return textoCondicional;
	}
	
}
