/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import br.com.massuda.alexander.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.persistencia.jdbc.model.Operacao;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;
import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
public class ComunicacaoChaveEstrangeira1ParaNEscrita extends Classe {
	
	
	private Class<?> classeGenerica;

	public void incluir(Object o, Field campo) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		List<?> inseridos = inserirFK(o, campo);
		realizarRelacionamentoFK(o, campo);
	}

	private List<?> inserirFK(Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, ErroUsuario, SQLException {
		List<?> lista = (List) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
		classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(classeGenerica) {};
		List inseridos = new ArrayList<>();
		for (Object param : lista) {
			boolean existe = existeFK(param);
			if (!existe) {
				Object paramIncluido = dao.incluir(param);
				inseridos.add(paramIncluido);
			}
		}
		return inseridos;
	}

	private boolean existeFK(Object param) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		FinderJDBC<?> finder = new FinderJDBC<>(param.getClass());
		Long id = (Long) getMetodo(param.getClass(), TipoMetodo.GET, "id").invoke(param);
		if (ObjectUtils.isEmpty(id)) {
			return false;
		}
		Object fk = finder.pesquisar(id);
		return ObjectUtils.isNotEmpty(fk);
	}

	private void realizarRelacionamentoFK(Object o, Field campo)
			throws IllegalAccessException, InvocationTargetException {
		String sqlEntidadeFraca;
		DAOGenericoJDBCImpl daoFK = new DAOGenericoJDBCImpl(classeGenerica) {};
		List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
		for (Object object : listaParam) {
			long id = (long)getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
			long idFK = (long)getMetodo(object.getClass(), TipoMetodo.GET, "id").invoke(object);
			sqlEntidadeFraca = new StringBuilder(GeradorSQLBeanFactory.get(classeGenerica).UPDATE_SIMPLES).append(GeradorSQLBeanFactory.get(classeGenerica).getNomeTabela()).append(" set ")
					.append(o.getClass().getSimpleName()).append("_id").append(" = ").append(id)
					.append(" where id = ").append(idFK).toString();
			daoFK.editar(sqlEntidadeFraca);
		}
	}
	
	public void alterar(Object o, Field campo) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		boolean ehSoRelacao = ehSoRelacao(o, campo);
		classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(classeGenerica) {};
		String sqlEntidadeEstrangeira;
		List<?> lista = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
		
		FinderJDBC<?> finder = new FinderJDBC<>(classeGenerica, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
		List inseridos = new ArrayList<>();
		if (!ehSoRelacao) {
			for (Object param : lista) {
				Long id = (Long) getMetodo(classeGenerica, TipoMetodo.GET, "id").invoke(param);
				Object paramIncluido = finder.pesquisar(id);
				if (ObjectUtils.isEmpty(paramIncluido)) {
					paramIncluido = dao.incluir(param);
				} else if (!param.equals(paramIncluido)) {
					dao.editar(param);
				}
				inseridos.add(paramIncluido);
			}
		}

		
		for (Object inserido : lista) {
			sqlEntidadeEstrangeira = GeradorSQLBeanFactory.get(classeGenerica).getComandoSelecao();
			Object idFK = getMetodo(classeGenerica, TipoMetodo.GET, "id").invoke(inserido);
			List<Filtro> filtros = new ArrayList<>();
			Filtro f = new Filtro(Long.class, o.getClass().getSimpleName()+"_id", TipoFiltro.IGUAL_A, idFK, true);
			filtros.add(f);
			ResultSet resultadoEntidadeFraca = finder.pesquisar(sqlEntidadeEstrangeira, filtros );
			if (ObjectUtils.isNotEmpty(resultadoEntidadeFraca) && !resultadoEntidadeFraca.next()) {
				Object fk = finder.pesquisar(resultadoEntidadeFraca.getLong("id"));
				sqlEntidadeEstrangeira = GeradorSQLBean.getInstancia(campo.getType()).getComandoAtualizacao(fk);
				dao.incluir(sqlEntidadeEstrangeira, o, inserido);		
			}
		}
	}
	

	public static boolean ehSoRelacao(Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		Class<?> classeGenerica = null;
		List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o, null);
		if (existe(listaParam)) {
			classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
			FinderJDBC<?> finder = new FinderJDBC(classeGenerica, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
			Long id = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o, null);
			List<Filtro> filtros = new ArrayList<Filtro>();
			String atributoNome = new StringBuilder(o.getClass().getSimpleName().toLowerCase()).append("_").append("id").toString();
			filtros.add(new Filtro(Long.class, atributoNome, TipoFiltro.IGUAL_A, id, true));
		
			List listaEntidadeEstrangeira = finder.pesquisar(filtros);
			if (ObjectUtils.isNotEmpty(listaEntidadeEstrangeira) && ObjectUtils.isNotEmpty(listaParam)
				 && listaParam.size() == listaEntidadeEstrangeira.size()) {
					return true;
			}
		}
		return false;
	}

	public void excluir(Object o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(o.getClass()) {};
		classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
		List<?> lista = (List) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
		
		FinderJDBC<?> finder = new FinderJDBC<>(classeGenerica);
		for (Object param : lista) {
			Long id = (Long) getMetodo(classeGenerica, TipoMetodo.GET, "id").invoke(param);
			Object fk = finder.pesquisar(id);
			dao.excluir(fk);
		}
		
	}
	
}
