/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author Alex
 *
 */
public class ComunicacaoTipoInteiro extends ComunicacaoJDBC {

	public ComunicacaoTipoInteiro() {
		this.SQL_VALOR_NULO = Types.INTEGER;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
			comandoPreparado.setInt(indice, 		(existe(valor) ? (Integer) valor : null));
	}

	void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado)
			throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		setter.invoke(o, resultado.getInt(campo.getName()));
	}

}
