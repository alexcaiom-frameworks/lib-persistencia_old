package br.com.massuda.alexander.persistencia.jdbc.model;

import java.io.Serializable;

import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChavePrimaria;

public class EntidadeModelo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1250376475990233589L;
	@ChavePrimaria(autoIncremental=true)
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
