package br.com.massuda.alexander.persistencia.jdbc.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.enums.JDBCDriver;
import br.com.waiso.framework.abstratas.Classe;

public class GerenciadorConexaoJDBC extends Classe {
	
	private static final String TIPO_CONEXAO_BD_JDBC = "jdbc";
	private static Connection conexao = null;
	
	public static Connection novaConexao() throws SQLException, ClassNotFoundException{
		return novaConexaoEm(ConstantesPersistencia.BD_DRIVER, TIPO_CONEXAO_BD_JDBC, ConstantesPersistencia.TIPO_BD_MySQL, ConstantesPersistencia.BD_CONEXAO_LOCAL, ConstantesPersistencia.BD_CONEXAO_PORTA, ConstantesPersistencia.BD_CONEXAO_NOME_BD,
				ConstantesPersistencia.BD_CONEXAO_USUARIO, ConstantesPersistencia.BD_CONEXAO_SENHA);
	}
	
	public static Connection novaConexaoSemBD() throws SQLException, ClassNotFoundException{
		return novaConexaoEm(ConstantesPersistencia.BD_DRIVER, TIPO_CONEXAO_BD_JDBC, ConstantesPersistencia.TIPO_BD_MySQL, ConstantesPersistencia.BD_CONEXAO_LOCAL, ConstantesPersistencia.BD_CONEXAO_PORTA, "",
				ConstantesPersistencia.BD_CONEXAO_USUARIO, ConstantesPersistencia.BD_CONEXAO_SENHA);
	}
	
	private static Connection novaConexaoEm(JDBCDriver driver, String tipoConexao, String nomeDoSGBD, String local, int porta, String nomeBD,
			String nomeUsuario, String senhaUsuario) throws SQLException, ClassNotFoundException{
		Class.forName (driver.getClasse());
		StringBuilder url = new StringBuilder()
				.append(tipoConexao).append(":").append(nomeDoSGBD).append("://")
				.append(local).append(":").append(porta);
		if (!StringUtils.isEmpty(nomeBD)) {
			url = url.append("/").append(nomeBD).append("?useSSL=false");
		}			
		DriverManager.setLoginTimeout(0);
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url.toString(), nomeUsuario, senhaUsuario);
		}catch (SQLException e) {
			System.err.println(e.getMessage());
			if (e.getMessage().contains("Unknown database ") || e.getMessage().contains("Access denied")) {
				StringBuilder urlSemBd = new StringBuilder().append(tipoConexao).append(":")
						.append(nomeDoSGBD).append("://")
						.append(local).append(":").append(porta).append("?useSSL=false");
				connection = DriverManager.getConnection(urlSemBd.toString(), nomeUsuario, senhaUsuario);
				if (ObjectUtils.isNotEmpty(connection)) {
					System.out.println("Criando banco de dados: "+nomeBD);
					connection.createStatement().execute(GeradorSQLBean.criarBD(nomeBD));
					connection.close();
					connection = null;
				}
				connection = novaConexaoEm(driver, tipoConexao, nomeDoSGBD, local, porta, nomeBD, nomeUsuario, senhaUsuario);
			}
		}
		return connection;
	}
	
	public static Connection getConexaoSemBD() throws SQLException {
		if (naoExiste(conexao) || conexao.isClosed()) {
			try {
				setConexao(novaConexaoSemBD());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return conexao;
	}

	public static Connection getConexao() throws SQLException {
		if (naoExiste(conexao) || conexao.isClosed()) {
			try {
				setConexao(novaConexao());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return conexao;
	}

	public static void setConexao(Connection conexao) {
		GerenciadorConexaoJDBC.conexao = conexao;
	}
	
	public static void fecharConexao() throws SQLException{
		if (existe(conexao) && !conexao.isClosed()) {
			conexao.close();
			conexao = null;
		}
	}
	
}