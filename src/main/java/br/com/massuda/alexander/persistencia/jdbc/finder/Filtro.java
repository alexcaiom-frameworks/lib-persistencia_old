package br.com.massuda.alexander.persistencia.jdbc.finder;

import java.lang.reflect.Field;

public class Filtro {

	private Field atributo;
	private String atributoNome;
	private TipoFiltro tipo;
	private Class atributoTipo;
	private Object valor;
	private boolean positivo;

	public Filtro() {}
	public Filtro(Field atributo, TipoFiltro tipo, Object valor, boolean positivo) {
		this.atributo = atributo;
		this.tipo = tipo;
		this.valor = valor;
		this.positivo = positivo;
	}
	public Filtro(Class atributoTipo, String atributoNome, TipoFiltro tipo, Object valor, boolean positivo) {
		this.atributoTipo = atributoTipo;
		this.atributoNome = atributoNome;
		this.tipo = tipo;
		this.valor = valor;
		this.positivo = positivo;
	}
	
	public Field getAtributo() {
		return atributo;
	}
	public Filtro setAtributo(Field atributo) {
		this.atributo = atributo;
		return this;
	}
	public String getAtributoNome() {
		return atributoNome;
	}

	public Filtro setAtributoNome(String atributoNome) {
		this.atributoNome = atributoNome;
		return this;
	}

	public TipoFiltro getTipo() {
		return tipo;
	}
	public Filtro setTipo(TipoFiltro tipo) {
		this.tipo = tipo;
		return this;
	}
	public Class getAtributoTipo() {
		return atributoTipo;
	}
	public void setAtributoTipo(Class atributoTipo) {
		this.atributoTipo = atributoTipo;
	}
	public Object getValor() {
		return valor;
	}
	public Filtro setValor(Object valor) {
		this.valor = valor;
		return this;
	}
	public boolean isPositivo() {
		return positivo;
	}
	public Filtro setPositivo(boolean positivo) {
		this.positivo = positivo;
		return this;
	}
	@Override
	public String toString() {
		return "Filtro [atributo=" + atributo + ", atributoNome=" + atributoNome  +", valor=" + valor + ", tipo=" + tipo + ", atributoTipo="
				+ atributoTipo +  ", positivo=" + positivo + "]";
	}
	
}
