/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc;

/**
 * @author Alex
 *
 */
public enum Regras {
	// Determinando code para cada enum
    Spock(1, Movimento.Spock, new Movimento[] {Movimento.Tesoura, Movimento.Pedra}, 
    		new String[] {"O Spock quebra a tesoura", "O Spock vaporiza a pedra"}),
    Lagarto(2, Movimento.Lagarto, new Movimento[] {Movimento.Spock, Movimento.Papel}, 
    		new String[] {"O lagarto envenena o Spock", "O lagarto come o papel"}),
    Pedra(3, Movimento.Pedra, new Movimento[] {Movimento.Lagarto, Movimento.Tesoura}, 
    		new String[] {"A pedra esmaga o lagarto", "A pedra quebra a tesoura"}),
    Tesoura(4, Movimento.Tesoura, new Movimento[] {Movimento.Papel, Movimento.Lagarto}, 
    		new String[] {"A tesoura corta papel", "A tesoura mata o lagarto"}),
    Papel(5, Movimento.Papel, new Movimento[] {Movimento.Pedra, Movimento.Spock}, 
    		new String[] {"O papel cobre a pedra", "O papel refuta o Spock"});

    // Codigo do tipo enumerado
    private int code;
	private Movimento[] ganhaDe;
	private String[] mensagens;
	private Movimento correspondente;
    
    // Construtor do tipo enumerado
    private Regras(int code, Movimento correspondente, Movimento[] ganhaDe, String[] mensagens) {
        this.code = code;
        this.correspondente = correspondente;
        this.ganhaDe = ganhaDe;
        this.mensagens = mensagens;
    }
    // Getter
    public int getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }

    // Metodo estatico para converter um valor numero para o tipo enumerado
    public static Regras valueOf(int code) {
        for (Regras value : values()) {
            if (value.getCode() == code) {
                return value;
            }
        }
        throw new IllegalArgumentException("Invalid OrderStatus Code");
    }
	public Movimento[] getGanhaDe() {
		return ganhaDe;
	}
	public String[] getMensagens() {
		return mensagens;
	}
	public Movimento getCorrespondente() {
		return correspondente;
	}
	
	/**
	 * 
	 * @return inteiro representando ordem da pessoa que ganhou (1 = 1o / 2 = 2o)
	 */
	public static Resultado quemGanha(Movimento movimento1, Movimento movimento2) {
		Resultado resultado = new Resultado();
		for (Regras regra : values()) {
			if (regra.getCorrespondente() == movimento1) {
				for (int indice = 0; indice < regra.getGanhaDe().length; indice++) {
					if (regra.ganhaDe[indice] == movimento2) {
						resultado.resultado = 1;
						resultado.mensagem = regra.mensagens[indice];
						return resultado;
					}
				}
			}
		}
		resultado.resultado = 2;
		return resultado;
	}
	
}

