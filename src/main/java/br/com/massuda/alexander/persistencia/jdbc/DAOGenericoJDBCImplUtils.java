package br.com.massuda.alexander.persistencia.jdbc;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import br.com.massuda.alexander.persistencia.jdbc.extratores.ComunicacaoChaveEstrangeira1ParaNEscrita;
import br.com.massuda.alexander.persistencia.jdbc.extratores.ComunicacaoChaveEstrangeiraNParaN;
import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.finder.Filtro;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoDeDado;
import br.com.massuda.alexander.persistencia.jdbc.finder.TipoFiltro;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;
import br.com.massuda.alexander.persistencia.jdbc.model.Operacao;
import br.com.massuda.alexander.persistencia.jdbc.testes.dao.Finder;
import br.com.massuda.alexander.persistencia.jdbc.utils.MetodoDeEntidade;
import br.com.massuda.alexander.persistencia.jdbc.utils.ParametroParaComandoParametrizado;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.TipoMetodo;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;
import br.com.waiso.framework.exceptions.ErroObjetoNaoPreenchido;

public abstract class DAOGenericoJDBCImplUtils<T> extends DAOJDBC {

	protected Class<T> entidade;
	public boolean possuiHeranca = false;
	

	public ResultSet executarOperacaoERetornarChavesGeradas(PreparedStatement comandoPreparado) throws SQLException {
		if (null != comandoPreparado) {
			try {
				System.out.println(comandoPreparado);
				comandoPreparado.execute();
				return comandoPreparado.getGeneratedKeys();
			} catch (Exception e) {
				if (e.getMessage().contains("Table") && e.getMessage().contains("doesn't exist")) {
					criarTabelaEDependencias();
					comandoPreparado.execute();
					return comandoPreparado.getGeneratedKeys();
				}
			}
		}
		return null;
	}

	/**
	 */
	protected void criarTabelaEDependencias() {
		criarDependencias(entidade);
	}

	private void criarDependencias(Class<T> entidade) {
		List<Field> camposFK = GeradorSQLBeanFactory.get(entidade).getCamposFKLista();
		camposFK.addAll(GeradorSQLBeanFactory.get(entidade).getCamposFKObjeto());
		
		for (Field campo : camposFK) {
			Class tipo = null;
			if (List.class.equals(campo.getType())) {
				tipo = ReflectionUtils.getTipoCampoGenerico(campo);
			} else {
				tipo = campo.getType();
			}
			if (!existeTabela(tipo)) {
				criarDependencias(tipo);
			}
		}
		if (GeradorSQLBean.getInstancia(entidade).deveCriarTabela()) {
			String sqlCriacao = GeradorSQLBean.getInstancia(entidade).getComandoTabelaCriacao();
			try {
				PreparedStatement comandoCriacao = novoComandoPreparado(sqlCriacao);
				executarOperacaoParametrizada(comandoCriacao);
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
	}

	private boolean existeTabela(Class entidade) {
		String sqlSelect = GeradorSQLBeanFactory.get(entidade).getComandoSelecao();
		try {
			getConexao().createStatement().executeQuery(sqlSelect);
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	
	protected boolean setarId(T o, Long chaveGerada, Class classe) {
		boolean sucesso = false;
		try {
			if (naoExiste(classe)) {
				classe = o.getClass();
			}
			Method metodoSetId;
			metodoSetId = getMetodo(o.getClass(), TipoMetodo.SET, "id");
			metodoSetId.invoke(o, chaveGerada);
			sucesso = true;
			
			if (naoExiste(metodoSetId)) {
				if (possuiHeranca && GeradorSQLBean.possuiHeranca(o.getClass())) {
					classe = ((Class) o.getClass()).getSuperclass();
					sucesso = setarId(o, chaveGerada, classe);
				} else {
					sucesso = false;
				}
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return sucesso;
	}

	protected boolean temAtributoIdEditavel(T o) {
		return setarId(o, 1l, null);
	}
	
	public void preencherParametrosDeComandoDeInsercao(PreparedStatement comandoPreparado, T o) throws SQLException {

		Field[] campos = GeradorSQLBeanFactory.get(entidade).getCamposDML();

		Map<Method, MetodoDeEntidade> gettersDosAtributos = new HashMap<Method, MetodoDeEntidade>();
		//Primeiro, verificamos os campos e se existe heranca
		for (int i = 0; i < campos.length; i++) {
			Field campo  = campos[i];
			if (GeradorSQLBeanFactory.get(entidade).ehChavePrimariaAutoIncremental(campo)) {
				continue;
			}
			try {
				boolean chaveEstrangeira = GeradorSQLBeanFactory.get(entidade).campoEhChaveEstrangeira(campo);
				Method metodoTemp = getMetodo(entidade, TipoMetodo.GET, campo.getName());
				MetodoDeEntidade metodo = new MetodoDeEntidade(false, chaveEstrangeira, metodoTemp, o);
				metodo.setCampoDoMetodo(campo);
				gettersDosAtributos.put(metodoTemp, metodo);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		
		if (possuiHeranca) {
			addCamposHerdadosAListaDeMetodosGetters(o.getClass(), gettersDosAtributos);
		}
		
		//Depois, iteramos os getters ja verificados e pegamos seus valores
		MetodoDeEntidade[] metodos = gettersDosAtributos.values().toArray(new MetodoDeEntidade[gettersDosAtributos.size()]);
		for (int i=0; i < metodos.length; i++) {
			MetodoDeEntidade metodo = metodos[i];
			if (GeradorSQLBeanFactory.get(entidade).ehChaveEstrangeira(metodo.getCampoDoMetodo())) {
				continue;
			}
			Object valor = null;
			try {
				Class tipo = null;
				valor = metodo.chamar(o);
				tipo = metodo.get().getReturnType();
				if (metodo.isChaveEstrangeira()) {
					Method m = tipo.getMethod("getId");
					if (existe(valor)) {
						valor = m.invoke(valor);
					}
					tipo = m.getReturnType();
				}
				int indiceQuery = qualIndiceInsercao(metodo);
				extrator.setParametroDeComandoPreparadoPorTipo(comandoPreparado, tipo, valor, indiceQuery+1);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void addCamposHerdadosAListaDeMetodosGetters(Class classeASerVerificada, Map<Method, MetodoDeEntidade> gettersDosAtributos) {
		Class mae = classeASerVerificada.getSuperclass();
		
		Field[] campos = GeradorSQLBean.getInstancia(mae).getCampos();
		for (int i = 0; i < campos.length; i++) {
			Field campo  = campos[i];
			boolean ehChavePrimariaAutoIncremental = GeradorSQLBean.getInstancia(mae).ehChavePrimariaAutoIncremental(campo);
			if (ehChavePrimariaAutoIncremental && tipoCRUD == OperacaoCRUD.INSERCAO) {
				continue;
			}
			String campoGetter = (GeradorSQLBean.getInstancia(classeASerVerificada.getClass()).getCampoNormal(campo));
			Character primeiroCaracter = campoGetter.charAt(0);
			campoGetter = "get"+primeiroCaracter.toUpperCase(primeiroCaracter)+campoGetter.substring(1);
			boolean chavePrimaria = GeradorSQLBean.getInstancia(mae).ehChavePrimaria(campo);
			boolean chaveEstrangeira = GeradorSQLBean.getInstancia(mae).ehChaveEstrangeira(campo);
			try {
				Method metodoTemp = mae.getMethod(campoGetter);
				MetodoDeEntidade metodo = new MetodoDeEntidade(chavePrimaria, chaveEstrangeira, metodoTemp);
				metodo.setCampoDoMetodo(campo);
				gettersDosAtributos.put(metodoTemp, metodo);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				System.out.println("O Metodo nao existe ou esta com outra nomenclatura. Implemente o getter com a IDE Eclipse");
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		
		if (GeradorSQLBean.possuiHeranca(mae)) {
			addCamposHerdadosAListaDeMetodosGetters(mae, gettersDosAtributos);
		}
	}
	
	protected void preencherEdicoes(PreparedStatement comandoPreparado, T o) throws SQLException {
		Field[] campos = GeradorSQLBeanFactory.get(entidade).getCamposDML();

		Map<Method, MetodoDeEntidade> gettersDosAtributos = new HashMap<Method, MetodoDeEntidade>();
		//Primeiro, verificamos os campos e se existe heranca
		for (int i = 0; i < campos.length; i++) {
			Field campo  = campos[i];
			if (GeradorSQLBean.getInstancia(o.getClass()).ehChaveEstrangeira(campo)) {
				continue;
			}
			try {
				boolean chaveEstrangeira = GeradorSQLBean.getInstancia(o.getClass()).ehChaveEstrangeira(campo);
				MetodoDeEntidade metodo = null;

				Method metodoTemp = getMetodo(entidade, TipoMetodo.GET, campo.getName());
				metodo = new MetodoDeEntidade(false, chaveEstrangeira, metodoTemp, o);
				metodo.setCampoDoMetodo(campo);
				gettersDosAtributos.put(metodoTemp, metodo);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		
		if (possuiHeranca) {
			addCamposHerdadosAListaDeMetodosGetters(o.getClass(), gettersDosAtributos);
		}
		
		//Depois, iteramos os getters ja verificados e pegamos seus valores
		MetodoDeEntidade[] metodos = gettersDosAtributos.values().toArray(new MetodoDeEntidade[gettersDosAtributos.size()]);
		
		for (int i=0, indiceQuery=0; i < metodos.length; i++, indiceQuery++) {
			MetodoDeEntidade metodo = metodos[i];
			Object valor = null;
			try {
				Class<?> tipo = null;
				valor = metodo.chamar(o);
				tipo = metodo.get().getReturnType();
				if (metodo.isChaveEstrangeira()) {
					indiceQuery--;
					continue;
				}
				indiceQuery = qualIndiceAtualizacao(metodo);
				
				extrator.setParametroDeComandoPreparadoPorTipo(comandoPreparado, tipo, valor, indiceQuery+1);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	
	private int qualIndiceInsercao(MetodoDeEntidade metodo) {
		String[] posicoesQuery = sql.substring(sql.indexOf("(")+1, sql.indexOf(")")).trim().split("\\,");
		for (int indice = 0; indice < posicoesQuery.length; indice++) {
			if (GeradorSQLBeanFactory.get(entidade).getCampo(metodo.getCampoDoMetodo(), false).equals(posicoesQuery[indice].trim())) {
				return indice;
			}
		}
		return 0;
	}
	
	private int qualIndiceAtualizacao(MetodoDeEntidade metodo) {
		String[] posicoesQuery = sql.substring(sql.lastIndexOf("set ")+"set ".length()).replace("=", "").replace("?", "").replace("where", "").replace(",", "").trim().split("\\n");
		for (int indice = 0; indice < posicoesQuery.length; indice++) {
			if (metodo.getCampoDoMetodo().getName().equals(posicoesQuery[indice].trim())) {
				return indice;
			}
		}
		return 0;
	}

	protected void preencherFiltros(PreparedStatement comandoPreparado, T o, List<ParametroParaComandoParametrizado> filtros) throws SQLException {
		Class<?> classe = o.getClass();
		Map<Method, MetodoDeEntidade> gettersDosAtributos = new HashMap<>();
		for (Field campo : GeradorSQLBeanFactory.get(entidade).getCampos()) {
			if (GeradorSQLBean.getInstancia(classe).ehChavePrimaria(campo) 
				|| GeradorSQLBean.getInstancia(classe).ehChaveEstrangeira(campo)) {
				
				MetodoDeEntidade metodo = null;

				String campoGetter = (GeradorSQLBean.getInstancia(o.getClass()).getCampoNormal(campo));
				
				Method metodoTemp;
				try {
					metodoTemp = getMetodo(entidade, TipoMetodo.GET, campoGetter);
					metodo = new MetodoDeEntidade(false, false, metodoTemp, o);
					gettersDosAtributos.put(metodoTemp, metodo);
				} catch (SecurityException e) {
					e.printStackTrace();
				}
			}
		}
		if (possuiHeranca) {
			addCamposHerdadosAListaDeMetodosGetters(o.getClass(), gettersDosAtributos);
		}
		
		//Depois, iteramos os getters ja verificados e pegamos seus valores
		MetodoDeEntidade[] metodos = gettersDosAtributos.values().toArray(new MetodoDeEntidade[gettersDosAtributos.size()]);
		for (int i=0, indiceQuery = 0; i < metodos.length; i++, indiceQuery++) {
			MetodoDeEntidade metodo = metodos[i];
			Object valor = null;
			try {
				Class tipo = null;
				valor = metodo.chamar(o);
				tipo = metodo.get().getReturnType();
				if (metodo.isChaveEstrangeira()) {
					indiceQuery--;
					continue;
				}

				extrator.setParametroDeComandoPreparadoPorTipo(comandoPreparado, tipo, valor, indiceQuery+1);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	

	public void preencherExclusao(PreparedStatement comandoPreparado, T o) {
		Method metodoGetId;
		try {
			metodoGetId = getMetodo(o.getClass(), TipoMetodo.GET, "id");
			if (existe(metodoGetId)) {
				Long id = (Long) metodoGetId.invoke(o, null);
				comandoPreparado.setLong(1, id);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public void addCamposHerdadosAListaDeMetodosSetters(Class classeASerVerificada, List<Method> settersDosAtributos, List<Field> atributos) {
		Class entidadeMae = classeASerVerificada.getSuperclass();
		
		Field[] campos = entidadeMae.getDeclaredFields();
		for (int i = 0; i < campos.length; i++) {
			Field campo  = campos[i];
			Class tipoCampo = campo.getType();
			if ("serialVersionUID".equals(campo.getName())) {
				continue;
			}
			
			try {
				Method metodo = getMetodo(entidadeMae, TipoMetodo.SET, campo.getName());
				settersDosAtributos.add(metodo);
				atributos.add(campo);
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		
		if (GeradorSQLBean.possuiHeranca(entidadeMae)) {
			addCamposHerdadosAListaDeMetodosSetters(entidadeMae, settersDosAtributos, atributos);
		}
	}

	protected void verificaSeOModeloEstaPreenchido(EntidadeModelo objeto)  {
		if (naoExiste(objeto)) {
			throw new ErroObjetoNaoPreenchido(objeto);
		}
	}
	

	protected void preencherParametrosDeComandoComFiltros(PreparedStatement comandoPreparado, List<Filtro> filtros) throws SQLException {
		for (int i = 0; i < filtros.size(); i++) {
			Filtro filtro = filtros.get(i);
			Object valor = filtro.getValor();
			Class tipo = filtro.getAtributoTipo();
			if (ObjectUtils.isEmpty(tipo)) {
				tipo = valor.getClass();
			}
			setParametroDeComandoPreparadoPorTipo(comandoPreparado, tipo, valor, i+1);
		}
	}	
	
	public void setParametroDeComandoPreparadoPorTipo(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		TipoDeDado tipoDeDado = TipoDeDado.get(tipo);
		switch (tipoDeDado) {
		case INTEGER:
			comandoPreparado.setInt(indice, 		(existe(valor) ? (Integer) valor : null));
			break;
		case LONG:
			comandoPreparado.setLong(indice, (Long) valor);
			break;
		case DOUBLE:
			comandoPreparado.setDouble(indice, 		(existe(valor) ? (Double) valor : null));
			break;
		case FLOAT:
			comandoPreparado.setFloat(indice, 		(existe(valor) ? (Float) valor : null));
			break;
		case BOOLEAN:
			int v = (existe(valor) ? (Boolean) valor : false) ? 1 : 0;
			comandoPreparado.setInt(indice, 		v);
			break;
		case STRING:
			comandoPreparado.setString(indice, 		(String) valor);
			break;
		case CALENDAR:
			java.sql.Date dataBD = null;
			if (tipo == Calendar.class) {
				Calendar data = (Calendar)valor;
				dataBD = new java.sql.Date(data.getTimeInMillis());
			} else if (tipo == Date.class) {
				Date data = (Date)valor;
				dataBD = new java.sql.Date(data.getTime());
			}
			comandoPreparado.setDate(indice, dataBD);
		case ENUM:
			comandoPreparado.setString(indice, 		(existe(valor) ? ((Enum)valor).name() : null));
		default:
			break;
		}
	}
	

	protected String parametrizarPesquisa(List<Filtro> filtros) {
		StringBuilder clausulaFiltro = new StringBuilder();
		
		boolean primeiro = true;
		boolean finalizarClausulaIn = false;
		for (Filtro filtro : filtros) {
			if(primeiro) {
				clausulaFiltro.append("\n where ");
			} else if (!finalizarClausulaIn) {
				clausulaFiltro.append("\n and ");
			}
			
			if (null != filtro.getAtributo()) {
				if (finalizarClausulaIn) {
					clausulaFiltro.append(" ) ");
				}
				clausulaFiltro.append(GeradorSQLBeanFactory.get(entidade).getCampoNormal(filtro.getAtributo()))
				.append(filtro.getTipo().getTextoCondicional()).append("?");
			} else if (StringUtils.isNotEmpty(filtro.getAtributoNome()) && TipoFiltro.EM != filtro.getTipo()) {
				if (finalizarClausulaIn) {
					clausulaFiltro.append(" ) ");
				}
				clausulaFiltro.append(filtro.getAtributoNome())
				.append(filtro.getTipo().getTextoCondicional()).append("?");
			} else if (StringUtils.isNotEmpty(filtro.getAtributoNome())) {
				clausulaFiltro.append(getParametrosNoIntervalo(clausulaFiltro, filtro));
			}
			
			finalizarClausulaIn = clausulaFiltro.toString().contains("(") && !clausulaFiltro.toString().contains(")");
			primeiro = false;
			if (isClausulaInFinalizada(filtros, clausulaFiltro) && finalizarClausulaIn) {
				clausulaFiltro.append(" ) ");
				finalizarClausulaIn = false;
			}
		}
		
		return clausulaFiltro.toString();
	}
	

	private boolean isClausulaInFinalizada(List<Filtro> filtros, StringBuilder clausulaFiltro) {
		long quantidadeFiltrosEm = getQuantidadeFiltrosEm(filtros);
		if (StringUtils.isNotBlank(clausulaFiltro) 
				&& clausulaFiltro.toString().contains("?")
				&& quantidadeFiltrosEm > 0
				&& StringUtils.countMatches(clausulaFiltro, '?') == quantidadeFiltrosEm) {
			return true;
		} 
//		else if (clausulaFiltro.toString().contains(",") 
//				&& clausulaFiltro.toString().split(",").length > 0) {
//			
//		}
		return false;
	}

	private long getQuantidadeFiltrosEm(List<Filtro> filtros) {
		long quantidade = 0;
		for (Filtro filtro : filtros) {
			if (TipoFiltro.EM.equals(filtro.getTipo())) {
				quantidade++;
			}
		}
		return quantidade;
	}

	private String getParametrosNoIntervalo(StringBuilder clausulaFiltro, Filtro filtro) {
		StringBuilder parametros = new StringBuilder();
		
		boolean primeiraExecucao = !clausulaFiltro.toString().contains("(");
		if (primeiraExecucao) {
			parametros.append(filtro.getAtributoNome())
			.append(filtro.getTipo().getTextoCondicional()).append(" (");
		} else {
			parametros.append(" , ");
		}
		parametros.append(" ?");
		return parametros.toString();
	}
	
	protected void processarPossiveisChavesEstrangeiras(T o) throws SQLException, NoSuchFieldException {
		boolean temChavesEstrangeiras1Para1 = GeradorSQLBeanFactory.get(entidade).temChavesEstrangeirasObjetos();
		boolean temChavesEstrangeirasListas = GeradorSQLBeanFactory.get(entidade).temChavesEstrangeirasListas();
		if (temChavesEstrangeiras1Para1) {
			try {
				processarChavesEstrangeirasObjetos(o);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| SecurityException e) {
				e.printStackTrace();
			}
		}
		if (temChavesEstrangeirasListas) {
			try {
				processarChavesEstrangeirasListas(o);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| SecurityException e) {
				e.printStackTrace();
			}
		} 
	}

	private void processarChavesEstrangeirasListas(T o) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException, NoSuchFieldException {
		List<Field> camposFK = GeradorSQLBean.getInstancia(entidade).getCamposFKLista();
		
		for (Field campo : camposFK) {
			final Operacao operacao = decidirOperacaoLista(o, campo);
			executarOperacaoCRUDLista(o, campo, operacao);
		}
	}
	
	private void processarChavesEstrangeirasObjetos(T o) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		List<Field> camposFK = GeradorSQLBeanFactory.get(entidade).getCamposFKObjeto();
		
		for (Field campo : camposFK) {
			Operacao operacao = decidirOperacaoObjeto(o, campo);
			executarOperacaoCRUDObjeto(o, campo, operacao);
		}
	}

	/**
	 * <b>Passos de decisao da Operacao</b> <br/>
	 * 1- Pegar o campo e efetuar um select informando a chave da entidade fraca. <br/>
	 * 2- Verificar o resultado do passo 1 <br/>
	 * 2.1- Se o resultado tiver dados, devemos verificar se os dados batem com os informados. <br/>
	 * Se o resultado nao bater, temos uma {@see Operacao.ALTERAR} do resultado da entidade fraca. <br/>
	 * 2.2- Se o resultado nao tiver dados, temos uma {@see Operacao.INSERIR} na entidade fraca. <br/>
	 * 3- Se os dados informados estiverem vazios, deveremos verificar se volta algo da consulta. <br/>
	 * Caso volte, teremos uma {@see Operacao.EXCLUIR}. <br/>
	 * @param o
	 * @param campo
	 * @return
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SQLException 
	 */
	private Operacao decidirOperacaoLista(T o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		Class<?> classeGenerica = null;
		
		try {
			List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o, null);
			if (existe(listaParam)) {
				classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
				boolean nParaN = verificaSeAListaEhNParaN(o, classeGenerica);
				FinderJDBC<?> finder = new FinderJDBC(classeGenerica, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS);
				
				List<Filtro> filtros = new ArrayList<Filtro>();
				if (nParaN) {
					ResultSet resultadoEntidadeFraca = finder.pesquisar(o, campo);
					List<Long> listaEntidadeFraca = new ArrayList<>();
					
					while (ObjectUtils.isNotEmpty(resultadoEntidadeFraca) && resultadoEntidadeFraca.next()) {
						listaEntidadeFraca.add(resultadoEntidadeFraca.getLong(classeGenerica.getSimpleName().toLowerCase()+"_id"));
					}
					
					List listaEntidadeEstrangeira = null;
					if (ObjectUtils.isNotEmpty(listaEntidadeFraca)) {
						filtros.clear();
						for (Long itemEntidadeFraca : listaEntidadeFraca) {
							Filtro f = new Filtro(Long.class, "id", TipoFiltro.EM, itemEntidadeFraca, true);
							filtros.add(f);
						}
						listaEntidadeEstrangeira = finder.pesquisar(filtros);
					}
					
					if (ObjectUtils.isNotEmpty(listaEntidadeEstrangeira)) {
						if (ObjectUtils.isNotEmpty(listaParam)) {
							if (listaParam.size() == listaEntidadeEstrangeira.size() && listaParam.containsAll(listaEntidadeEstrangeira)) {
								return Operacao.get(true, true, true);
							}
							return Operacao.get(true, true, false);
						}
						return Operacao.get(false, true, false);
					}
					return Operacao.get(true, false, false);
				} else {
					List listaEntidadeEstrangeira = finder.pesquisar(filtros);
					if (ObjectUtils.isNotEmpty(listaParam)) {
						if (ObjectUtils.isNotEmpty(listaEntidadeEstrangeira)) {
							if (listaEntidadeEstrangeira.containsAll(listaParam)) {
								return Operacao.get(true, true, true);
							}
							return Operacao.get(true, true, false);
						}
						return Operacao.get(true, false, false);
					}
					return Operacao.get(false, false, false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Operacao.get(false, false, false);
	}
	
	private boolean verificaSeAListaEhNParaN(Object o, Class<?> tipoGenericoLista) {
		Field[] atributos = tipoGenericoLista.getDeclaredFields();
		boolean nParaN = false;
		for (Field atributo : atributos) {
			if (atributo.getClass().equals(o.getClass()) 
					|| List.class.equals(atributo.getType())
						&& ReflectionUtils.getTipoCampoGenerico(atributo).equals(o.getClass())) {
				nParaN = true;
			}
		}
		return nParaN;
	}

	private Operacao decidirOperacaoObjeto(T o, Field campo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		
		try {
			Object obj = getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
			if (existe(obj)) {
				Long idChaveEstrangeira = (Long) getMetodo(obj.getClass(), TipoMetodo.GET, "id").invoke(obj);
				
				if (existe(idChaveEstrangeira)) {
					if (!GeradorSQLBeanFactory.get(entidade).tabelaEstaNoMesmoBD(campo)) {
						return Operacao.get(true, true, true);
					}
					Finder finder = new Finder(campo.getType()) {};
					Object entidadeEstrangeira = finder.pesquisar(idChaveEstrangeira);
					if (existe(entidadeEstrangeira)) {
						if (obj.equals(entidadeEstrangeira)) {
							return Operacao.get(true, true, true);
						}
						return Operacao.get(true, true, false);
					}
				} else if (temAlgoPreenchido(obj)) {
					return Operacao.get(true, false, false);
				}
			} else {
				Object id = getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
				FinderJDBC<?> finder = new FinderJDBC<>(entidade);
				Object dbObject = finder.pesquisar((Long) id);
				if (ObjectUtils.isNotEmpty(dbObject)) {
					Object fk = getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(dbObject);
					if (ObjectUtils.isNotEmpty(fk)) {
						return Operacao.get(false, true, false);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Operacao.get(false, false, false);
	}
	
	private boolean temAlgoPreenchido(Object obj) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		boolean algoPreenchido = false;
		for (Field campo : obj.getClass().getDeclaredFields()) {
			if ("serialVersionUID".equals(campo.getName())) continue;
			Object valor = getMetodo(obj.getClass(), TipoMetodo.GET, campo.getName()).invoke(obj, null);
			if (!campo.getType().isEnum() && existe(valor)) {
				algoPreenchido = true;
			}
		}
		return algoPreenchido;
	}

	private void executarOperacaoCRUDLista(T o, Field campo, Operacao operacao) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException, NoSuchFieldException {
		Class<?> classeGenerica = null;
		if (null != operacao) {
			List<?> listaParam = (List<?>) getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o, null);
			if (existe(listaParam)) classeGenerica = ReflectionUtils.getTipoCampoGenerico(campo);
			boolean ehNParaN = verificaSeAListaEhNParaN(o, classeGenerica);
			switch (operacao) {
			case INSERIR:
				if (ehNParaN) {
					new ComunicacaoChaveEstrangeiraNParaN().incluir(o, campo);
				} else {
					new ComunicacaoChaveEstrangeira1ParaNEscrita().incluir(o, campo);
				}
				break;
			case ALTERAR:
				if (ehNParaN) {
					new ComunicacaoChaveEstrangeiraNParaN().alterar(o, campo);
				} else {
					new ComunicacaoChaveEstrangeira1ParaNEscrita().alterar(o, campo);
				}
				break;
			case EXCLUIR:
				if (ehNParaN) {
					new ComunicacaoChaveEstrangeiraNParaN().excluir(o, campo);
				} else {
					new ComunicacaoChaveEstrangeira1ParaNEscrita().excluir(o, campo);
				}
				
				break;
			default:
				break;
			}
		}
		
	}
	
	private void executarOperacaoCRUDObjeto(T o, Field campo, Operacao operacao) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, SQLException {
		if (null != operacao) {
			DAOGenericoJDBCImpl dao = new DAOGenericoJDBCImpl(campo.getType()) {};
			Object objetoExtrangeiro = getMetodo(o.getClass(), TipoMetodo.GET, campo.getName()).invoke(o);
			Object objectId;
			StringBuilder sqlBuilder = null;
			Long indiceParametro = null;
			switch (operacao) {
			case INSERIR:
				Object objetoIncluido = dao.incluir(objetoExtrangeiro);
				Object indiceIncluido = getMetodo(objetoIncluido.getClass(), TipoMetodo.GET, "id").invoke(objetoIncluido);
				sqlBuilder = GeradorSQLBeanFactory.get(entidade).getComandoInicial(GeradorSQLBeanFactory.get(entidade).UPDATE_SIMPLES);
				sqlBuilder.append("\n SET ").append(GeradorSQLBeanFactory.get(campo.getType()).getCampo(campo, false)).append(" = ?")
											.append("\n where id = ?");
				comandoPreparado = novoComandoPreparado(sqlBuilder.toString());
				comandoPreparado.setLong(1, (long) indiceIncluido);
				indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
				comandoPreparado.setLong(2, indiceParametro);
				executarOperacaoParametrizada(comandoPreparado);
				break;
			case ALTERAR:
				objectId = getMetodo(campo.getType(), TipoMetodo.GET, "id").invoke(o);
				if (ObjectUtils.isNotEmpty(objectId)) {
					Object fk = new FinderJDBC<>(campo.getType()).pesquisar((long) objectId);
					Object fkId = getMetodo(fk.getClass(), TipoMetodo.GET, "id").invoke(fk);
					if (ObjectUtils.isNotEmpty(fk) && !fk.equals(objetoExtrangeiro)) {
						BeanUtils.copyProperties(objetoExtrangeiro, fk);
						new DAOGenericoJDBCImpl(campo.getType()) {}.editar(fk);
					}
					sqlBuilder = GeradorSQLBeanFactory.get(entidade).getComandoInicial(GeradorSQLBeanFactory.get(entidade).UPDATE_SIMPLES);
					sqlBuilder.append("\n SET ").append(GeradorSQLBeanFactory.get(campo.getType()).getCampo(campo, false))
										.append(" = ?").append("\n where id = ?");
					comandoPreparado = novoComandoPreparado(sqlBuilder.toString());
					comandoPreparado.setLong(1, (long) fkId);
					indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
					comandoPreparado.setLong(2, indiceParametro);
					executarOperacaoParametrizada(comandoPreparado);
				}
				break;
			case EXCLUIR:
				objectId = getMetodo(campo.getType(), TipoMetodo.GET, "id").invoke(o);
				if (ObjectUtils.isNotEmpty(objectId)) {
					sql = new StringBuilder(GeradorSQLBeanFactory.get(entidade).getComandoInicial(GeradorSQLBeanFactory.get(entidade).UPDATE_SIMPLES))
							.append("\n SET ").append(GeradorSQLBeanFactory.get(entidade).getCampo(campo, false)).append(" = null")
							.append("\n where ").append("id = ?").toString();
					PreparedStatement ps = novoComandoPreparado(sql);
					ps.setLong(1, (long) objectId);
					executarOperacaoParametrizada(ps);
				}
				
				break;
			case NADA:
				
				objectId = getMetodo(campo.getType(), TipoMetodo.GET, "id").invoke(objetoExtrangeiro);
				if (ObjectUtils.isNotEmpty(objectId)) {
					if (GeradorSQLBeanFactory.get(entidade).tabelaEstaNoMesmoBD(campo)) {
						Object fk = new FinderJDBC<>(campo.getType()).pesquisar((long) objectId);
						if (ObjectUtils.isNotEmpty(fk)) {
							Object fkId = getMetodo(fk.getClass(), TipoMetodo.GET, "id").invoke(fk);
							if (!fk.equals(objetoExtrangeiro)) {
								BeanUtils.copyProperties(objetoExtrangeiro, fk);
								new DAOGenericoJDBCImpl(campo.getType()) {}.editar(fk);
							}
							sqlBuilder = GeradorSQLBeanFactory.get(entidade).getComandoInicial(GeradorSQLBeanFactory.get(entidade).UPDATE_SIMPLES);
							sqlBuilder.append("\n SET ").append(GeradorSQLBeanFactory.get(campo.getType()).getCampo(campo, false))
							.append(" = ?").append("\n where id = ?");
							comandoPreparado = novoComandoPreparado(sqlBuilder.toString());
							comandoPreparado.setLong(1, (long) fkId);
							indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
							comandoPreparado.setLong(2, indiceParametro);
							executarOperacaoParametrizada(comandoPreparado);
						}
						
					} else {
						sqlBuilder = GeradorSQLBeanFactory.get(entidade).getComandoInicial(GeradorSQLBeanFactory.get(entidade).UPDATE_SIMPLES);
						sqlBuilder.append("\n SET ").append(GeradorSQLBeanFactory.get(campo.getType()).getCampo(campo, false))
						.append(" = ?").append("\n where id = ?");
						comandoPreparado = novoComandoPreparado(sqlBuilder.toString());
						comandoPreparado.setLong(1, (long) objectId);
						indiceParametro = (Long) getMetodo(o.getClass(), TipoMetodo.GET, "id").invoke(o);
						comandoPreparado.setLong(2, indiceParametro);
						executarOperacaoParametrizada(comandoPreparado);
						
					}
				}
			}
		}
		
	}

	protected void criarTabela(SQLSyntaxErrorException e) {
		if (e.getMessage().contains("Table") && e.getMessage().contains("doesn't exist")) {
			criarTabelaEDependencias();
		}
	}

}
