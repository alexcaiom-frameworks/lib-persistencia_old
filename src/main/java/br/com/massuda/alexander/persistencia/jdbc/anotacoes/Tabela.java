package br.com.massuda.alexander.persistencia.jdbc.anotacoes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import br.com.massuda.alexander.persistencia.jdbc.enums.Juncao;

@Retention(RetentionPolicy.RUNTIME)
public @interface Tabela {

	public String nome() default "";
	public boolean criar() default true;
	public Juncao juncao() default Juncao.INVERSA;
	
}