/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.model;

/**
 * @author alex
 *
 */
public enum Operacao {
	//		param, bd,   equals
	INSERIR(true, false, false),
	ALTERAR(true, true, false),
	EXCLUIR(false, true, false),
	NADA(true, true, true);
	
	private boolean existeParametro;
	private boolean existeBD;
	private boolean ehIgual;

	private Operacao(boolean existeParametro, boolean existeBD, boolean ehIgual) {
		this.existeParametro = existeParametro;
		this.existeBD = existeBD;
		this.ehIgual = ehIgual;
	}

	public boolean isExisteParametro() {
		return existeParametro;
	}

	public boolean isExisteBD() {
		return existeBD;
	}

	public boolean isEhIgual() {
		return ehIgual;
	}
	
	public static Operacao get(Boolean existeParametro, Boolean existeBD, Boolean ehIgual) {
		for (Operacao operacao : values()) {
			if (existeParametro.equals(operacao.existeParametro)
					&& existeBD.equals(operacao.existeBD)
					&& ehIgual.equals(operacao.ehIgual)) {
				return operacao;
			}
		}
		return null;
	}
	
}
