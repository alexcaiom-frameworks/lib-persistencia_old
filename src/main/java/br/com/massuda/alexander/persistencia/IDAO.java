package br.com.massuda.alexander.persistencia;

import java.sql.SQLException;

import br.com.waiso.framework.exceptions.ErroUsuario;

public interface IDAO<T> {

	public T incluir(T o) throws ErroUsuario, SQLException;
	public T editar(T o)  throws ErroUsuario, SQLException;
	public void excluir(T o)  throws ErroUsuario, SQLException;
	
}
