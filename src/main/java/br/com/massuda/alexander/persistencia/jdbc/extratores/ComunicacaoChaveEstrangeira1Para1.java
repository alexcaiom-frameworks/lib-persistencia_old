/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.lang3.ObjectUtils;

import br.com.massuda.alexander.persistencia.jdbc.GeradorSQLBean;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.TipoMetodo;

/**
 * @author Alex
 *
 */
public class ComunicacaoChaveEstrangeira1Para1 extends ComunicacaoJDBC {
	
	public ComunicacaoChaveEstrangeira1Para1() {
		this.SQL_VALOR_NULO = Types.VARCHAR;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		comandoPreparado.setString(indice, 		(String) valor);
	}

	public void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		setter.invoke(o, resultado.getString(campo.getName()));
		getChaveEstrangeira1Para1(resultado, campo.getType(), campo.getName(), nomeBD, o, anotado, true);
	}
	

	private void getChaveEstrangeira1Para1(ResultSet resultado, Class tipoCampo, String nome, String nomeBD, Object o,
			Boolean anotado, boolean temAtributoId) {
		boolean temHeranca = GeradorSQLBean.possuiHeranca(tipoCampo);
		try {
			//FIXME este trecho de Codigo nao cobre herancas profundas
			if (temHeranca) {
				Class mae = tipoCampo.getSuperclass();
				while(GeradorSQLBean.possuiHeranca(mae)){
					mae = mae.getSuperclass();
				}
				temAtributoId = existe(mae.getDeclaredField("id"));
			} else {
				temAtributoId = existe(tipoCampo.getDeclaredField("id"));
			}
			//FIXME este trecho de Codigo nao cobre herancas profundas
		}
		catch (NoSuchFieldException e) {
			e.printStackTrace(); 
		}
		catch (SecurityException e) {  
			e.printStackTrace();  
		}
		
		if (temAtributoId) {
			Object entidadeRelacionada;
			try {
				entidadeRelacionada = tipoCampo.newInstance();
				FinderJDBC<?> finderFK = new FinderJDBC(tipoCampo, TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS); 
				
				String sufixo_id = (!anotado) ? "_id" : "";
				nomeBD = nomeBD.isEmpty() ? nome + sufixo_id : nomeBD;
				Long id = resultado.getLong(nomeBD);
				
				entidadeRelacionada = finderFK.pesquisar(id);
				if (ObjectUtils.isNotEmpty(entidadeRelacionada)) {
					getMetodo(entidadeRelacionada.getClass(), TipoMetodo.SET, "id").invoke(entidadeRelacionada, id);
				}
				
//				Method metodoSetEntidade = null;
//				try {//Tentamos obter o setter da chave estrangeira. Se for o mesmo tipo de entidade, podemos usar o modo normal.
//					metodoSetEntidade =  o.getClass().getMethod("set"+tipoCampo.getSimpleName(), tipoCampo);
//				} catch (NoSuchMethodException eSemMetodo){
//					metodoSetEntidade = o.getClass().getMethod("set"+nome.substring(0, 1).toString().toUpperCase()+nome.substring(1), tipoCampo);
//				}
				
				getMetodo(o.getClass(), TipoMetodo.SET, nome).invoke(o, entidadeRelacionada);
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//			} catch (NoSuchMethodException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
