/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author Alex
 *
 */
public class ComunicacaoTipoBoolean extends ComunicacaoJDBC {
	
	private static final int VALOR_PADRAO_INTEIRO_TRUE = 1;

	public ComunicacaoTipoBoolean() {
		this.SQL_VALOR_NULO = Types.BOOLEAN;
	}

	public void input(PreparedStatement comandoPreparado, Class tipo, Object valor, int indice) throws SQLException {
		int v = (existe(valor) ? ((Boolean) valor): false) ? 1 : 0;
		comandoPreparado.setInt(indice, v);
	}
	
	public void output(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, Boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Integer v = resultado.getInt(nomeBD);
		
		boolean flag = (existe(v) && VALOR_PADRAO_INTEIRO_TRUE==v) ;
		setter.invoke(o, flag);
	}

}
