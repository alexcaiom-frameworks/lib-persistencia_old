/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.enums;

/**
 * @author Alex
 *
 */
public enum JDBCDriver {
	
	MYSQL("mysql", "com.mysql.jdbc.Driver", ""),
	ORACLE("oracle", "oracle.jdbc.driver.OracleDriver", "xe"),
	MS_SQL_SERVER("sqlserver", "oracle.jdbc.driver.OracleDriver", ""),
	POSTGRE_SQL("", "oracle.jdbc.driver.OracleDriver", "");
	
	private String tipo;
	private String classe;
	private String instancia;

	private JDBCDriver(String tipo, String classe, String instancia) {
		this.tipo = tipo;
		this.classe = classe;
		this.instancia = instancia;
	}

	public String getClasse() {
		return classe;
	}

	public String getTipo() {
		return tipo;
	}
	
	public String getInstancia() {
		return instancia;
	}

	public static JDBCDriver get(String nome) {
		for (JDBCDriver driver: values()) {
			if (driver.tipo.equals(nome)) {
				return driver;
			}
		}
		return null;
	}

}
