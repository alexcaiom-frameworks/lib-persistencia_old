package br.com.massuda.alexander.persistencia.jdbc.anotacoes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia;

@Retention(RetentionPolicy.RUNTIME)
public @interface Coluna {

	public String nome() default "NULL";
	public boolean anulavel() default true;
	public boolean unica() default false;
	public int tamanho() default ConstantesPersistencia.BANCO_DE_DADOS_TIPO_DADO_STRING_TAMANHO_PADRAO;
	public boolean deveSerPersistida() default true;
	
}