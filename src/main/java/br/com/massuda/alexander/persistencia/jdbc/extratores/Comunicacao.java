
/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.extratores;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.abstratas.utils.ReflectionUtils;

/**
 * @author Alex
 *
 */
public class Comunicacao extends Classe {

	private List<ComunicacaoJDBC> comunicadores;

	public Comunicacao() {
		comunicadores = new ArrayList<>();
		comunicadores.add(new ComunicacaoTipoBoolean());
		comunicadores.add(new ComunicacaoTipoData());
		comunicadores.add(new ComunicacaoTipoDouble());
		comunicadores.add(new ComunicacaoTipoEnum());
		comunicadores.add(new ComunicacaoTipoFloat());
		comunicadores.add(new ComunicacaoTipoInteiro());
		comunicadores.add(new ComunicacaoTipoLong());
		comunicadores.add(new ComunicacaoTipoString());
	}

	public void setParametroDeComandoPreparadoPorTipo(PreparedStatement comandoPreparado, Class<?> tipo, Object valor, int indice) throws SQLException {
		ComunicacaoJDBC extrator = JDBCTypes.selecionar(comunicadores, tipo);
		if (existe(valor)) {
			extrator.input(comandoPreparado, tipo, valor, indice);
		} else {
			comandoPreparado.setNull(indice, extrator.SQL_VALOR_NULO);
		}
	}

	public void getValorDaPesquisa(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, boolean anotado) throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		try {
			ComunicacaoJDBC extrator = JDBCTypes.selecionar(comunicadores, campo.getType());
			if (existe(extrator)) {
				extrator.output(resultado, campo, nomeBD, setter, o, anotado);
			} 
		} catch (IllegalArgumentException e) {
			if (JDBCTypes.CHAVE_ESTRANGEIRA.equals(e.getMessage())) {
				tratarChaveEstrangeira(resultado, campo, nomeBD, setter, o, anotado);
			} else  
				throw e;

		}
	}

	private void tratarChaveEstrangeira(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, boolean anotado) {
		Class<?> tipoCampo = campo.getType();
		String nome = campo.getName();
		try {
			if (existe(tipoCampo) && (tipoCampo.getName().startsWith("com") || tipoCampo.getName().startsWith("br")) && !tipoCampo.getClass().isEnum()) {
				System.out.println("Chave estrangeira: " + nome);
				new ComunicacaoChaveEstrangeira1Para1().output(resultado, campo, nomeBD, setter, o, anotado);
			} else if (tipoCampo.isInterface() && List.class.equals(tipoCampo)) {
				System.out.println("Chave estrangeira: " + nome);
				tratarMParaN(resultado, campo, nomeBD, setter, o, anotado);
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException e) {
			e.printStackTrace();
		}

	}

	private void tratarMParaN(ResultSet resultado, Field campo, String nomeBD, Method setter, Object o, boolean anotado)
			throws SQLException, IllegalAccessException, InvocationTargetException {
		Class<?> parametro = ReflectionUtils.getTipoCampoGenerico(campo);
		Field[] atributos = parametro.getDeclaredFields();
		boolean nParaN = false;
		for (Field atributo : atributos) {
			if (atributo.getClass().equals(o.getClass()) 
					|| List.class.equals(atributo.getType())
						&& ReflectionUtils.getTipoCampoGenerico(atributo).equals(o.getClass())) {
				nParaN = true;
			}
		}
		
		if (nParaN) {
			new ComunicacaoChaveEstrangeiraNParaN().output(resultado, campo, nomeBD, setter, o, anotado);
		} else {
			new ComunicacaoChaveEstrangeira1ParaN().output(resultado, campo, nomeBD, setter, o, anotado);
		}
	}

}
