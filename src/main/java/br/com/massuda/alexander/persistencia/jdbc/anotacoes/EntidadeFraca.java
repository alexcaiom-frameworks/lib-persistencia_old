/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.anotacoes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author massuda.alex
 *
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface EntidadeFraca {
	
	public String tabela() default "";
	public String associacao1() default "";
	public String associacao2() default "";

}
