/**
 * 
 */
package br.com.massuda.alexander.persistencia.jdbc.excecoes;

import java.sql.SQLException;

import br.com.waiso.framework.exceptions.ErroUsuario;

/**
 * @author Alex
 *
 */
public class RegistroJaExistenteException extends ErroUsuario {

	/**
	 * 
	 */
	private static final long serialVersionUID = 39071116071601832L;

	private String codigo;
	private String mensagem;
	private Exception origem;
	
	public RegistroJaExistenteException(SQLException e) {
		super(e.getMessage());
		this.mensagem = e.getMessage();
		this.origem = e;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Exception getOrigem() {
		return origem;
	}
	public void setOrigem(Exception origem) {
		this.origem = origem;
	}
	
}
